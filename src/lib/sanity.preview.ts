"use client";
import { definePreview } from "next-sanity/preview";
import { projectId, dataset } from "./sanity";
export const usePreview = definePreview({ projectId, dataset });
