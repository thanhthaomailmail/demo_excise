import sanityClient from "@sanity/client";
export const projectId = process.env.SANITY_PROJECT_ID || "vgn335dt";
export const dataset = process.env.SANITY_DATASET_NAME || "production";
const apiVersion = process.env.SANITY_API_VERSION || "2023-04-10";
const options = {
  dataset: "production",
  projectId: "vgn335dt",
  useCdn: process.env.NODE_ENV === "production",
  apiVersion: apiVersion,
};
export default sanityClient(options);
