import sanityClient from "./sanity";
import { FieldsInput } from "./field";
import imageUrlBuilder from "@sanity/image-url";

export async function fetchInputField(slug: any) {
  const result = await sanityClient
    .fetch(
      `*[_type == "header_blog_input" && slug.current == $slug ]{${FieldsInput}}`,
      { slug }
    )
    .then((res) => res?.[0]);
  return result;
}

export async function fetchDataHomePage() {
  const result = await sanityClient.fetch(
    `*[_type =="header_blog_input"]{${FieldsInput}}`
  );
  return result;
}
const builder = imageUrlBuilder(sanityClient);

export function urlFor(source: any) {
  return builder.image(source);
}
// import sanityClient from "./sanity";
// export async function fetchInputField() {
//   const result = await sanityClient.fetch(
//     `*[_type == "header_blog_input"]{title,'background':background.asset->url, content, 'hashtag':hashtag->{
//       name, job
//     } ,time, description, 'author': author->{name, position, 'avatar'}}`
//   );

//   return result;
// }
