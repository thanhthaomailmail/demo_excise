// GLOBAL
export const globalBackground = "#F6F6F6";

// Primary color
export const brandColorDarker = "#F4B533";

// Secondary color
export const cream = "#FEF7E1";
export const brown = "#302406";
export const lightBrown = "#595145 ";
export const prussianBlue = "#002F4C";
export const white = "#FFFFFF";
//Grayscale color
export const gray1 = "#1D1400";
export const gray2 = "#483F2A";
export const gray3 = "#6A6352";
export const gray4 = "#868173";
export const gray5 = "#D7D4CF";
export const gray6 = "#EBEBEB";
export const gray8 = "#3F3F3F";
export const grayWhite = "#FFFFFF";
// Semantic colo
export const semanticRed = "#B30000";
export const semanticGreen = "#62CA70";
export const semanticOrange = "#F38E31";
export const semanticBlue = "#006ED6";

// Gradient color
export const gradient1 = "linear-gradient(90deg, #FFBC08 0%, #F4B533 100%)";
export const gradient2 =
  "linear-gradient(90deg, #1D1400 0%, rgba(29, 20, 0, 0) 100%)";
export const gradient3 = "linear-gradient(270deg, #FEAE2F 0%, #F9640A 100%)";
export const searchableSelectStyle = {
  singleValue: (provided: object) => {
    const color = lightBrown;
    return { ...provided, color };
  },
  option: (provided: any, state: any) => ({
    ...provided,
    color: state.isFocused ? cream : lightBrown,
    background: state.isFocused
      ? "linear-gradient(270deg, #FEAE2F 0%, #F9640A 100%)"
      : "transparent",
  }),
  control: () => ({
    border: "1px solid #EBEBEB !important",
    display: "flex !important",
    fontWeight: "500 !important",
    fontSize: "16px !important",
    borderRadius: "4px !important",
    height: "45px  !important",
    padding: "0 auto !important",
  }),

  indicatorSeparator: (provided: object) => {
    const background = "#EBEBEB !important";
    return { ...provided, background };
  },
  indicatorContainer: (provided: object) => {
    const background = "#EBEBEB !important";
    return { ...provided, background };
  },
};
export const inputDiallingCodeStyle = {
  height: "45px",
  width: "100%",
  border: "1px solid #EBEBEB",
  borderRadius: "4px",
  color: "#302406",
  fontSize: "16px",
  fontWeight: 500,
};
