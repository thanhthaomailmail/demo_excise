import { extendTheme, theme as baseTheme } from "@chakra-ui/react";
import { theme as proTheme } from "@chakra-ui/pro-theme";
import { modalAnatomy as parts } from "@chakra-ui/anatomy";
import { createMultiStyleConfigHelpers } from "@chakra-ui/styled-system";
import {
  brandColorDarker,
  brown,
  cream,
  globalBackground,
  gradient1,
  gradient2,
  gradient3,
  gray1,
  gray4,
  gray5,
  gray6,
  grayWhite,
  lightBrown,
  prussianBlue,
  semanticBlue,
  semanticGreen,
  semanticOrange,
  semanticRed,
  white,
} from "./globalStyles";

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(parts.keys);

const baseStyle = definePartsStyle({
  dialog: {
    borderRadius: "8px",
    background: "#FFFFFF",
    w: "424px",
    h: "617px",
    shadow: "0px 8px 16px rgba(48, 36, 6, 0.1)",
  },
  body: {
    p: "24px",
  },
});

const profileInformationRefreshModal = definePartsStyle({
  dialog: {
    borderRadius: "4px",
    background: "#FFFFFF",
    border: `1px #EBEBEB`,
    w: "146px",
    h: "111px",
  },
  body: {
    p: "16px",
  },
});
const mainNavBarModal = definePartsStyle({
  overlay: {
    bg: "#302406 !important",
    backdropFilter: "auto",
    opacity: "0.5 !important",
  },
  dialogContainer: {
    w: "100%",
  },
  dialog: {
    background: "#FFFFFF",
    rounded: "4px",
    maxW: { base: "344px", lg: "824px", xl: "1136px !important" },
    width: { base: "344px", lg: "824px", xl: "1136px !important" },
    height: "fit-content",
    shadow: "0px 3px 6px rgba(48, 36, 6, 0.1);",
    marginTop: "0",
  },

  body: {
    p: "0",
    w: "inherit",
  },
});
const blogSearchingModal = definePartsStyle({
  overlay: {
    bg: "#302406 !important",
    backdropFilter: "auto",
    opacity: "0.5 !important",
  },
  dialogContainer: {
    w: "100%",
  },
  dialog: {
    background: "#FFFFFF",
    rounded: "4px",
    maxW: { base: "344px", lg: "824px", xl: "1136px !important" },
    width: { base: "344px", lg: "824px", xl: "1136px !important" },
    height: "fit-content",
    shadow: "0px 3px 6px rgba(48, 36, 6, 0.1);",
  },

  body: {
    p: "0",
    w: "inherit",
  },
});
const customerFeedbacksModal = definePartsStyle({
  overlay: {
    bg: "#302406 !important",
    backdropFilter: "auto",
    opacity: "0.5",
  },
  dialogContainer: {
    w: "100%",
  },
  dialog: {
    rounded: "4px",
    maxW: "293px !important",
    width: "293px !important",
    height: "521px",
    shadow: "unset",
  },
  body: {
    p: "0",
    w: "inherit",
  },
});
const discountModal = definePartsStyle({
  overlay: {
    bg: "#302406 !important",
    backdropFilter: "auto",
    opacity: "0.5 !important",
  },

  dialog: {
    borderRadius: "8px",
    background: "#FFFFFF",
    border: "none",
    rounded: "8px",
    maxW: { base: "328px", lg: "424px" },
    height: "fit-content",
    padding: 0,
  },

  body: {
    maxW: { base: "328px", lg: "424px" },
    mx: { base: "16px", lg: "24px", xl: "32px" },
    padding: 0,
  },
});
const confirmDetailsModal = definePartsStyle({
  overlay: {
    bg: "#302406 !important",
    backdropFilter: "auto",
    opacity: "0.5 !important",
  },

  dialog: {
    background: "#FFFFFF",
    rounded: "8px",
    maxW: { base: "344px", lg: "368px" },
    height: "fit-content",
    padding: 0,
    shadow: "0px 8px 16px rgba(48, 36, 6, 0.1)",
  },

  body: {
    maxW: { base: "320px", lg: "320px" },
    mx: { base: "12px", lg: "24px" },
    padding: 0,
  },
});
export const modalTheme = defineMultiStyleConfig({
  variants: {
    profileInformationRefreshModal,
    blogSearchingModal,
    discountModal,
    baseStyle,
    confirmDetailsModal,
    mainNavBarModal,
    customerFeedbacksModal,
  },
});

const theme = extendTheme(
  {
    //! Theme Color Config
    initialColorMode: "light",

    //! Breakpoints Config
    breakpoints: {
      //mobile
      base: "360px",
      sm: "640px",
      //tablet
      md: "768px",
      //desktop
      lg: "1024px",
      //desktop
      xl: "1200px",
    },

    //! Spacing Config
    space: {
      "-1": "0",
      0: "0",
      1: "1rem",
      2: "1.5rem",
      5: "2.5rem",
      8: "4rem",
      13: "6.5rem",
      21: "10.5rem",
      35: "16.5rem",
    },

    //! TextStyle Config (textStyle)
    textStyles: {
      h1: {
        fontSize: "72px",
        fontWeight: 600,
        lineHeight: "108px",
      },
      h2: {
        fontSize: "64px",
        fontWeight: 600,
        lineHeight: "100px",
      },
      h3: {
        fontSize: "56px",
        fontWeight: 600,
        lineHeight: "84px",
      },
      h4: {
        fontSize: "46px",
        fontWeight: 600,
        lineHeight: "69px",
      },
      h5: {
        fontSize: "32px",
        fontWeight: 600,
        lineHeight: "48px",
      },
      h6: {
        fontSize: "24px",
        fontWeight: 600,
        lineHeight: "36px",
      },
      h7: {
        fontSize: "16px",
        fontWeight: 600,
        lineHeight: "24px",
      },
      h8: {
        fontSize: "16px",
        fontWeight: 500,
        lineHeight: "30px",
      },

      bodyText1: {
        fontSize: "18px",
        fontWeight: 500,
        lineHeight: "26px",
      },
      bodyText2: {
        fontSize: "16px",
        fontWeight: 500,
        lineHeight: "24px",
      },
      bodyText3: {
        fontSize: "14px",
        fontWeight: 500,
        lineHeight: "21px",
      },
      bodyText4: {
        fontSize: "12px",
        fontWeight: 500,
        lineHeight: "18px",
      },
      bodyText5: {
        fontSize: "18px",
        fontWeight: 600,
        lineHeight: "27px",
      },
      tooltip: {
        fontSize: "10px",
        fontWeight: 500,
        lineHeight: "15px",
      },
      hr: {
        width: "100%",
        textAlign: "center",
        borderBottom: "1px",
        borderStyle: "solid",
        borderColor: gray1,
        lineHeight: 0,
      },
      textGradient: {
        background: gradient3,
        color: "transparent",
        bgClip: "text",
        fontSize: "18px",
        lineHeight: "27px",
        fontWeight: 600,
      },
    },

    //! Component Config
    components: {
      //! Heading Config
      Heading: {
        sizes: {
          h1: {
            fontSize: "72px",
            fontWeight: 600,
            lineHeight: "108px",
          },
          h2: {
            fontSize: "64px",
            fontWeight: 600,
            lineHeight: "100px",
          },
          h3: {
            fontSize: "56px",
            fontWeight: 600,
            lineHeight: "84px",
          },
          h4: {
            fontSize: "46px",
            fontWeight: 600,
            lineHeight: "69px",
          },
          h5: {
            fontSize: "32px",
            fontWeight: 600,
            lineHeight: "48px",
          },
          h6: {
            fontSize: "24px",
            fontWeight: 600,
            lineHeight: "36px",
          },
          h7: {
            fontSize: "16px",
            fontWeight: 600,
            lineHeight: "24px",
          },
        },
      },

      //! NumberInput Config
      NumberInput: {
        baseStyle: {
          width: "80px !important",
          _disabled: {
            background: "#F2F2F2",
          },
          _checked: {
            background: grayWhite,
          },
        },
        field: {
          width: "80px !important",
        },
        defaultProps: {
          outline: "none !important",
          boxShadow: "none !important",
          focusBorderColor: "none",
        },
      },

      //! CheckBox Config
      Checkbox: {
        baseStyle: {
          control: {
            w: "24px",
            h: "24px",
            borderRadius: "4px",
            borderColor: lightBrown,
            outline: "none !important",
            boxShadow: "none !important",
            _checked: {
              background: semanticGreen,
              border: "none",
              borderColor: "none",
              _hover: {
                background: semanticGreen,
                border: "none",
                borderColor: "none",
              },
            },
          },
        },
      },

      //!Button Config
      Button: {
        sizes: {
          default: {
            height: "45px",
            width: "151px",
            fontSize: "16px",
            lineHeight: "30px",
            fontWeight: 600,
            border: "1px solid transparent",
            rounded: "4px",
          },
        },

        variants: {
          semanticOrange: {
            background: "transparent",
            border: "1px solid",
            borderColor: "outline.semanticOrange",
            color: "color.brandColorDarker",
          },
          transparent: {
            background: "transparent",
            border: "none",
            outline: `1px ${prussianBlue} `,
          },
          brandColorDarker: {
            background: "background.brandColorDarker",
            border: "none",
            color: "color.cream",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          gradient1: {
            background: "background.gradient1",
            border: "none",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          gradient2: {
            background: "background.gradient2",
            border: "none",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          gradient3: {
            background: "background.gradient3",
            color: "color.cream",
            border: "none",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          prussianBlue: {
            background: "background.prussianBlue",
            color: "color.cream",
            border: "none",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          grayscaleWhite: {
            background: "background.grayWhite",
            color: "color.brandColorDarker",
            borderColor: "outline.brandColorDarker",
            border: "1px solid",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
            },
          },
          cream: {
            background: "background.cream",
            color: "color.brown",
            border: "1px solid transparent",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
              color: "color.cream",
            },
          },
          brown: {
            background: "background.grayWhite",
            color: "color.brown",
            border: "1px solid",
            borderColor: "outline.brown",
            _hover: {
              background: "background.prussianBlue",
              border: "1px solid transparent",
              color: "color.cream",
            },
          },
          danger: {
            background: "background.danger",
            color: "color.cream",
            border: "1px solid transparent",
            _hover: {
              background: "background.prussianBlue",
            },
            _focus: {},
          },
          warning: {
            background: "transparent",
            color: "color.brandColorDarker",
            border: "1px solid",
            borderColor: "outline.brandColorDarker",
            _hover: {
              background: "background.prussianBlue",
              color: "color.cream",
              border: "1px solid transparent",
            },
            _focus: {},
          },
        },
        defaultProps: {
          size: "default",
        },
      },

      //! Input Config
      Input: {
        sizes: {
          default: {
            field: {
              height: "45px",
              fontSize: "16px",
              lineHeight: "30px",
              fontWeight: 600,
              borderRadius: "4px",
              padding: "0 16px",
              _disabled: {
                background: "#F7F7F7",
                opacity: 0.5,
              },
            },
          },
        },
        variants: {
          default: {
            field: {
              background: "background.grayWhite",
              border: "1px solid #EBEBEB",
              color: "color.brown",
              _placeholder: {
                color: "color.lightBrown",
                fontWeight: "500",
              },
              _invalid: {
                border: "1px solid",
                borderColor: "outline.red",
                shadow: "(#B30000) 0px 0px 0px 1px",
              },
            },
          },
          gray6: {
            field: {
              background: "background.gray6",
              color: "color.brown",
              fontWright: 600,
              _placeholder: {
                color: "color.lightBrown",
                fontWeight: "600",
              },
            },
          },
          prussianBlue: {
            field: {
              background: "transparent",
              border: `1px ${prussianBlue}`,
              color: "color.prussianBlue",
              _placeholder: {
                color: "color.prussianBlue",
                fontWeight: "500",
              },
            },
          },
        },
        defaultProps: {
          variant: "default",
          size: "default",
        },
      },

      //! Select Config
      Select: {
        baseStyle: {
          field: {
            border: "1px #CBC9C3",
            color: "secondary.lightBrown",
            _hover: {
              border: "1px #302406 !important",
            },
            _focus: {
              border: "1px #302406 !important",
              outline: "none !important",
              boxShadow: "none !important",
            },
            _autofill: {
              border: "1px #302406",
              textFillColor: "#302406",
              transition: "background-color 5000s ease-in-out 0s",
            },
            _defaultvalue: {
              color: "#595145 ",
              fontWeight: "500",
              fontSize: "1rem",
              lineHeight: "1.875rem",
            },
            _focusVisible: {
              borderColor: "#F4B533!important",
              boxShadow: "0 0 0 1px #F4B533!important",
            },
          },
        },
        sizes: {
          md: {
            field: {
              height: "45px",
            },
          },
        },
      },

      //! Form Config
      Form: {
        variants: {
          default: {
            container: {
              label: {
                mb: "8px",
                fontSize: "16px",
                fontWeight: 600,
                lineHeight: "24px",
                color: "color.brown",
              },
            },
          },
        },
      },

      //! Tooltip Config
      Tooltip: {
        baseStyle: {
          background: "background.brandColorDarker",
          color: "color.brown",
          rounded: "4px",
        },
      },

      //! Tag Config
      Tag: {
        variants: {
          default: {
            container: {
              height: "24px",
              width: "fit-content",
              background: "transparent",
              border: "1px solid !important",
              borderColor: "outline.brown",
            },
            closeButton: {
              display: "none",
            },
            label: {
              width: "fit-content",
            },
          },
          department: {
            container: {
              height: "24px",
              width: "fit-content",
              background: "transparent",
              border: "1px solid !important",
              borderColor: "outline.brown",
              rounded: "12px",
            },
            closeButton: {
              display: "none",
            },
            label: {
              width: "fit-content",
              color: "color.brown",
            },
          },
          tag: {
            container: {
              height: "24px",
              width: "fit-content",
              background: "background.brandColorDarker",
              border: "none",
              color: "color.cream",
            },
            closeButton: {
              background: "background.cream",
              opacity: 1,
              width: "14px",
              height: "14px",
              fontSize: "10px",
              color: "color.brandColorDarker",
            },
            label: {
              width: "fit-content",
            },
          },
        },
      },

      //! Tab Config
      Tabs: {
        variants: {
          orderInstruction: {
            tab: {
              fontSize: "16px",
              lineHeight: "24px",
              fontWeight: 600,
              border: "none",
            },
            tablist: {},
            tabpanel: {},
          },
        },
      },

      //! Modal Config
      Modal: modalTheme,
    },

    //! Styles  Config
    styles: {
      global: {
        "html, body": {
          background: "background.global",
          maxW: "100vw",
          fontSize: "16px",
        },
        a: {
          textDecor: "unset !important",
          fontSize: "16px",
          fontWeight: 500,
          lineHeight: "30px",
          color: "color.lightBrown",
          display: "block",
          _hover: {
            color: "color.brandColorDarker",
          },
        },
        th: {
          background: "transparent",
          color: brown,
          fontSize: "16px",
          fontWeight: "600",
          lineHeight: "24px",
          textAlign: "center",
        },
        td: {
          color: brown,
          fontSize: "16px",
          fontWeight: 500,
          lineHeight: "30px",
          textAlign: "center",
        },
        p: {
          color: "color.brown",
        },
      },
    },

    //! Colors Config
    colors: {
      //! For background
      background: {
        white: grayWhite,
        green: semanticGreen,
        global: globalBackground,
        brandColorDarker: brandColorDarker,
        gradient1: gradient1,
        gradient2: gradient2,
        gradient3: gradient3,
        prussianBlue: prussianBlue,
        cream: cream,
        grayWhite: grayWhite,
        lightBrown: lightBrown,
        gray6: gray6,
        gray5: gray5,
        danger: semanticRed,
      },

      //! For text
      color: {
        cream: cream,
        prussianBlue: prussianBlue,
        brown: brown,
        lightBrown: lightBrown,
        gray4: gray4,
        gray5: gray5,
        gradient3: gradient3,
        brandColorDarker: brandColorDarker,
        semanticGreen: semanticGreen,
        semanticBlue: semanticBlue,
        semanticRed: semanticRed,
        semanticOrange: semanticOrange,
        white: white,
      },

      //! For borderColor
      outline: {
        danger: semanticRed,
        success: semanticGreen,
        warning: semanticOrange,
        darker: prussianBlue,
        prussianBlue: prussianBlue,
        brandColorDarker: brandColorDarker,
        brown: brown,
        gray5: gray5,
        gray6: gray6,
        cream: cream,
        orange: semanticOrange,
        blue: semanticBlue,
        red: semanticRed,
      },
    },

    //! Font Config
    fonts: {
      heading: `Quicksand, ${baseTheme.fonts?.heading}`,
      body: `Quicksand, ${baseTheme.fonts?.body}`,
      mono: `Quicksand, ${baseTheme.fonts?.mono}`,
    },
  },
  proTheme
);

export default theme;
