export const animationNavbarRotate = () => {
  return {
    transform: "rotate(180deg)",
    fill: "color.brandColorDarker",
    transition: "0.2s linear",
  };
};
export const animationNavbarChangeColor = () => {
  return {
    color: "color.brandColorDarker",
  };
};
