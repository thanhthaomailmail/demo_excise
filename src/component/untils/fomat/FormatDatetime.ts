export const FormatDatetime = (d: Date) => {
  const monthly = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const date = d.getDate();
  const month = monthly[d.getMonth()];
  const hour = d.getHours();
  const min = d.getMinutes();
  const year = d.getFullYear();
  const dateFormatted = `${month} ${date},${year} at ${hour}:${min}`;
  return dateFormatted;
};
