import { Box, Center, HStack, Link, Text, VStack } from "@chakra-ui/layout";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Button,
  Input,
} from "@chakra-ui/react";
import { ImFacebook } from "react-icons/im";
import { IoLogoTwitter } from "react-icons/io";
import { AiFillLinkedin, AiFillYoutube } from "react-icons/ai";
const FooterBlog = () => {
  return (
    <Box mt="150px" mb="100px">
      <Box mx="20px" bg=" #f8f9fa !important">
        <VStack w="1136px" mx="auto" h="200px" alignItems={"start"}>
          <Text w="fit-content" fontWeight={"700"} mt="50px">
            Subscribe to newsletter
          </Text>
          <HStack spacing={30}>
            <Input
              placeholder="Enter Email"
              w="700px"
              h="50px"
              borderRadius={"10px"}
              _focus={{ border: "1px solid black" }}
            />
            <Button
              w="406px"
              h="50px"
              bgColor={"#F59A1E"}
              borderRadius={"50px"}
              _hover={{
                bgColor: "#FFFFFF",
              }}
              boxShadow={"0px 3px 6px rgba(48, 36, 6, 0.1)"}
            >
              Subcribe
            </Button>
          </HStack>
        </VStack>
      </Box>
      <VStack mt="100px">
        <Center>
          <Button
            w="40px"
            h="40px"
            mr="10px"
            borderRadius="15px"
            _hover={{
              bgColor: "#F59A1E",
              color: "#FFFFFF",
            }}
          >
            <ImFacebook />
          </Button>
          <Button
            w="40px"
            h="40px"
            mr="10px"
            borderRadius="15px"
            _hover={{
              bgColor: "#F59A1E",
              color: "#FFFFFF",
            }}
          >
            <IoLogoTwitter />
          </Button>
          <Button
            w="40px"
            h="40px"
            mr="10px"
            borderRadius="15px"
            _hover={{
              bgColor: "#F59A1E",
              color: "#FFFFFF",
            }}
          >
            <AiFillLinkedin />
          </Button>
          <Button
            w="40px"
            h="40px"
            mr="10px"
            borderRadius="15px"
            _hover={{
              bgColor: "#F59A1E",
              color: "#FFFFFF",
            }}
          >
            <AiFillYoutube />
          </Button>
        </Center>
        <Text fontWeight={"300"} fontSize={"18px"}>
          Copyright ©2023 All rights reserved
        </Text>
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink href="#">Home</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink href="#">Blog</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </VStack>
    </Box>
  );
};

export default FooterBlog;
