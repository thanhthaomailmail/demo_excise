import { Link } from "@chakra-ui/react";
const ExitPreviewLink = () => {
  return (
    <Link
      position="sticky"
      bottom="0"
      left="100px"
      zIndex={100}
      color="color.brandColorDarker"
      fontSize={"36px"}
      href="/api/exit-preview"
    >
      Exit Preview
    </Link>
  );
};

export default ExitPreviewLink;