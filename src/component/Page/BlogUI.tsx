import CardBlog from "../CardBlog/CardBlog";
import HeaderBlog from "../HeaderBlog/HeaderBlog";
import SliderBlog from "../SliderBlog/SliderBlog";
import MostPopularPost from "../MostPopularPost/MostPopularPost";
import FooterBlog from "../FooterBlog/FooterBlog";
import { Box } from "@chakra-ui/layout";
import { DataProps } from "@/pages/Blog";

const HomeUI = ({ data }: { data: DataProps[] }) => {
  return (
    <Box w="100%" bg="#fff">
      <HeaderBlog />

      {/* Slider */}
      <SliderBlog data={data} />
      {/* Card */}
      <CardBlog data={data} />
      <MostPopularPost />
      <FooterBlog />
    </Box>
  );
};

export default HomeUI;
