import { Box } from "@chakra-ui/react";
import { usePreview } from "../../lib/sanity.preview";
import ExitPreviewLink from "../Link/ExitPreviewLink";
import DetailBlog from "../DetailBlog/DetailBlog";
export default function HomePreview({
  query,
  queryParams,
}: {
  query: string;
  queryParams: { [key: string]: any };
}) {
  const data = usePreview(null, query, queryParams);
  return (
    <Box position="relative">
      <DetailBlog data={data[0]} />
      <ExitPreviewLink />
    </Box>
  );
}
