import { Box } from "@chakra-ui/react";
import { usePreview } from "../../lib/sanity.preview";
import ExitPreviewLink from "../Link/ExitPreviewLink";
import HomeUI from "../Page/BlogUI";
export default function HomePreview({ query }: { query: string }) {
  const data = usePreview(null, query);
  return (
    <Box position="relative">
      <HomeUI data={data} />
      <ExitPreviewLink />
    </Box>
  );
}
