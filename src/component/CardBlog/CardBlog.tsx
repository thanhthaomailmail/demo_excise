import { Box, HStack, Image, SimpleGrid, Text, VStack } from "@chakra-ui/react";
import { FormatDatetime } from "../untils/fomat/FormatDatetime";
import { useRouter } from "next/router";
import { DataProps } from "@/pages/Blog";

const CardBlog = ({ data }: { data: DataProps[] }) => {
  const router = useRouter();
  const push = router.push;
  return (
    <Box w="1136px" mx="auto" mt="40px">
      <SimpleGrid columns={3} spacing={50}>
        {data.map((item, index) => (
          <VStack
            cursor={"pointer"}
            onClick={() => {
              push(`/Blog/${item.slug}`);
            }}
            alignItems={"start"}
            spacing={4}
            w="350px"
            key={index}
          >
            <Image
              borderRadius={"10px"}
              src={item.background}
              alt="EmGai"
              h="250px"
              w="350px"
              objectFit={"-moz-initial"}
            />
            <HStack>
              <Text fontWeight="600" fontSize="14px">
                {item.hashtag.job}
              </Text>
              <Text>—</Text>
              <Text fontWeight="400" fontSize="14px" color={"#888"}>
                {FormatDatetime(new Date(item.time))}
              </Text>
            </HStack>
            <Text fontWeight="700" fontSize="20px" w="fit-content" h="60px">
              {item.title}
            </Text>
            <Text w="fit-content" fontWeight="400" fontSize="14px">
              {item.description}
            </Text>

            <HStack>
              <Image
                src={item.author.avatar}
                alt="animals"
                h="45px"
                w="45px"
                borderRadius={"50%"}
                objectFit={"cover"}
              />
              <VStack alignItems={"start"}>
                <Text fontWeight={"700"} lineHeight={"1"}>
                  {item.author.name}
                </Text>

                <Text fontSize={"13px"} color={"#888"}>
                  {item.author.position}
                </Text>
              </VStack>
            </HStack>
          </VStack>
        ))}

        {/* <VStack alignItems={"start"} spacing={4} w="350px">
          <Image
            borderRadius={"10px"}
            src="https://chocanh.vn/wp-content/uploads/cho-husky-sibir-ngao-2.jpg"
            alt="EmGai"
            h="250px"
            w="350px"
            objectFit={"-moz-initial"}
          />
          <HStack>
            <Text fontWeight="600" fontSize="14px">
              Business, Travel
            </Text>
            <Text fontWeight="600" fontSize="14px">
              — July 2, 2020
            </Text>
          </HStack>
          <Text fontWeight="700" fontSize="20px" w="fit-content">
            Your most unhappy customers are your greatest source of learning.
          </Text>
          <Text w="fit-content" fontWeight="400" fontSize="14px">
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia, there live the blind texts.
          </Text>

          <HStack>
            <Image
              src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
              alt="animals"
              h="45px"
              w="45px"
              borderRadius={"50%"}
              objectFit={"cover"}
            />
            <VStack alignItems={"start"}>
              <Text fontWeight={"700"} lineHeight={"1"}>
                Sergy Campbell
              </Text>

              <Text fontSize={"13px"} color={"#888"}>
                CEO and Founder
              </Text>
            </VStack>
          </HStack>
        </VStack>
        <VStack alignItems={"start"} spacing={4} w="350px">
          <Image
            borderRadius={"10px"}
            src="https://chocanh.vn/wp-content/uploads/cho-husky-sibir-ngao-2.jpg"
            alt="EmGai"
            h="250px"
            w="350px"
            objectFit={"-moz-initial"}
          />
          <HStack>
            <Text fontWeight="600" fontSize="14px">
              Business, Travel
            </Text>
            <Text fontWeight="600" fontSize="14px">
              — July 2, 2020
            </Text>
          </HStack>
          <Text fontWeight="700" fontSize="20px" w="fit-content">
            Your most unhappy customers are your greatest source of learning.
          </Text>
          <Text w="fit-content" fontWeight="400" fontSize="14px">
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia, there live the blind texts.
          </Text>

          <HStack>
            <Image
              src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
              alt="animals"
              h="45px"
              w="45px"
              borderRadius={"50%"}
              objectFit={"cover"}
            />
            <VStack alignItems={"start"}>
              <Text fontWeight={"700"} lineHeight={"1"}>
                Sergy Campbell
              </Text>

              <Text fontSize={"13px"} color={"#888"}>
                CEO and Founder
              </Text>
            </VStack>
          </HStack>
        </VStack>
        <VStack alignItems={"start"} spacing={4} w="350px">
          <Image
            borderRadius={"10px"}
            src="https://chocanh.vn/wp-content/uploads/cho-husky-sibir-ngao-2.jpg"
            alt="EmGai"
            h="250px"
            w="350px"
            objectFit={"-moz-initial"}
          />
          <HStack>
            <Text fontWeight="600" fontSize="14px">
              Business, Travel
            </Text>
            <Text fontWeight="600" fontSize="14px">
              — July 2, 2020
            </Text>
          </HStack>
          <Text fontWeight="700" fontSize="20px" w="fit-content">
            Your most unhappy customers are your greatest source of learning.
          </Text>
          <Text w="fit-content" fontWeight="400" fontSize="14px">
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia, there live the blind texts.
          </Text>

          <HStack>
            <Image
              src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
              alt="animals"
              h="45px"
              w="45px"
              borderRadius={"50%"}
              objectFit={"cover"}
            />
            <VStack alignItems={"start"}>
              <Text fontWeight={"700"} lineHeight={"1"}>
                Sergy Campbell
              </Text>

              <Text fontSize={"13px"} color={"#888"}>
                CEO and Founder
              </Text>
            </VStack>
          </HStack>
        </VStack>
        <VStack alignItems={"start"} spacing={4} w="350px">
          <Image
            borderRadius={"10px"}
            src="https://chocanh.vn/wp-content/uploads/cho-husky-sibir-ngao-2.jpg"
            alt="EmGai"
            h="250px"
            w="350px"
            objectFit={"-moz-initial"}
          />
          <HStack>
            <Text fontWeight="600" fontSize="14px">
              Business, Travel
            </Text>
            <Text fontWeight="600" fontSize="14px">
              — July 2, 2020
            </Text>
          </HStack>
          <Text fontWeight="700" fontSize="20px" w="fit-content">
            Your most unhappy customers are your greatest source of learning.
          </Text>
          <Text w="fit-content" fontWeight="400" fontSize="14px">
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia, there live the blind texts.
          </Text>

          <HStack>
            <Image
              src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
              alt="animals"
              h="45px"
              w="45px"
              borderRadius={"50%"}
              objectFit={"cover"}
            />
            <VStack alignItems={"start"}>
              <Text fontWeight={"700"} lineHeight={"1"}>
                Sergy Campbell
              </Text>

              <Text fontSize={"13px"} color={"#888"}>
                CEO and Founder
              </Text>
            </VStack>
          </HStack>
        </VStack>
        <VStack alignItems={"start"} spacing={4} w="350px">
          <Image
            borderRadius={"10px"}
            src="https://chocanh.vn/wp-content/uploads/cho-husky-sibir-ngao-2.jpg"
            alt="EmGai"
            h="250px"
            w="350px"
            objectFit={"-moz-initial"}
          />
          <HStack>
            <Text fontWeight="600" fontSize="14px">
              Business, Travel
            </Text>
            <Text fontWeight="600" fontSize="14px">
              — July 2, 2020
            </Text>
          </HStack>
          <Text fontWeight="700" fontSize="20px" w="fit-content">
            Your most unhappy customers are your greatest source of learning.
          </Text>
          <Text w="fit-content" fontWeight="400" fontSize="14px">
            Far far away, behind the word mountains, far from the countries
            Vokalia and Consonantia, there live the blind texts.
          </Text>

          <HStack>
            <Image
              src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
              alt="animals"
              h="45px"
              w="45px"
              borderRadius={"50%"}
              objectFit={"cover"}
            />
            <VStack alignItems={"start"}>
              <Text fontWeight={"700"} lineHeight={"1"}>
                Sergy Campbell
              </Text>

              <Text fontSize={"13px"} color={"#888"}>
                CEO and Founder
              </Text>
            </VStack>
          </HStack>
        </VStack> */}
      </SimpleGrid>
    </Box>
  );
};

export default CardBlog;
