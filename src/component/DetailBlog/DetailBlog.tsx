import {
  Box,
  Divider,
  SimpleGrid,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/layout";
import { Image } from "@chakra-ui/react";
import { FormatDatetime } from "../untils/fomat/FormatDatetime";
import { DataProps } from "@/pages/Blog";
import { urlFor } from "@/src/lib/api";
import FooterBlog from "../FooterBlog/FooterBlog";
// const BlockContent = require("@sanity/block-content-to-react");

const serializers = {
  types: {
    image: ({ node: { asset, alt } }: any) => {
      return (
        <Box>
          <Image
            alt="Blog Image"
            height="337.5px"
            width="600px"
            src={urlFor(asset).url()}
          />
          <Text textAlign={"center"} color="color.lightBrown">
            {alt}
          </Text>
        </Box>
      );
    },
  },
};
const DetailBlog = ({ data }: { data: DataProps }) => {
  return (
    <Box w="1136px" mx="auto" mt="30px">
      <VStack mt="4px" alignItems={"center"} mb="50px">
        <Image
          src={data.author.avatar}
          alt="animals"
          h="70px"
          w="70px"
          borderRadius={"50%"}
          objectFit={"cover"}
        />

        <Text
          fontWeight={"400"}
          lineHeight={"1"}
          fontSize={"18px"}
          color={"#888"}
        >
          {data.author.name}
        </Text>

        <Text fontWeight={"400"} lineHeight={"1.5"} color={"#888"}>
          {FormatDatetime(new Date(data.time))}
        </Text>
      </VStack>
      {/*  <Box id="blog__content">
            <BlockContent
              blocks={data.content}
              projectId="m0vfyvsr"
              dataset="production"
              // imageOptions={{ w: 600, h: 450 }}
              serializers={serializers}
            />
          </Box> */}
      <VStack spacing={4} alignItems={"start"}>
        <Text
          fontWeight={"700"}
          fontSize={"50px"}
          w="fit-content"
          textAlign={"center"}
        >
          Your most unhappy customers are your greatest source of learning.
        </Text>
        <Text
          fontWeight={"300"}
          fontSize={"1.5rem"}
          w="fit-content"
          color="#888"
          textAlign={"center"}
        >
          Far far away, behind the word mountains, far from the countries
          Vokalia and Consonantia, there live the blind texts.
        </Text>
        <Image
          borderRadius={"5px"}
          src="https://thuythithi.com/wp-content/uploads/2020/03/so-huu-doi-mat-ngao-ngo-chu-cho-gay-sot-mang.jpg"
          alt="animals"
          w="1000px"
          h="514px"
          objectFit="cover"
        />
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          Far far away, behind the word mountains, far from the countries
          Vokalia and Consonantia, there live the blind texts. Separated they
          live in Bookmarksgrove right at the coast of the Semantics, a large
          language ocean.
        </Text>
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          A small river named Duden flows by their place and supplies it with
          the necessary regelialia. It is a paradisematic country, in which
          roasted parts of sentences fly into your mouth.
        </Text>
      </VStack>
      <Stack direction="row" h="50px" mt="10px">
        <Divider
          orientation="vertical"
          color="black"
          w="20px"
          borderColor="none"
        />
        <Text fontWeight={"400"} fontSize={"18px"} lineHeight="1.5" pl="20px">
          The Big Oxmox advised her not to do so, because there were thousands
          of bad Commas, wild Question Marks and devious Semikoli, but the
          Little Blind Text didn’t listen. She packed her seven versalia, put
          her initial into the belt and made herself on the way.
        </Text>
      </Stack>
      <VStack mt="20px">
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          Even the all-powerful Pointing has no control about the blind texts it
          is an almost unorthographic life One day however a small line of blind
          text by the name of Lorem Ipsum decided to leave for the far World of
          Grammar.
        </Text>
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          When she reached the first hills of the Italic Mountains, she had a
          last view back on the skyline of her hometown Bookmarksgrove, the
          headline of Alphabet Village and the subline of her own road, the Line
          Lane. Pityful a rethoric question ran over her cheek, then she
          continued her way.
        </Text>
      </VStack>
      <SimpleGrid columns={3} spacing={1} my="60px">
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://mayvesinhmiennam.com/wp-content/uploads/2021/06/k3.jpg"
          alt="husky1"
        />
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://zda.vn/wp-content/uploads/2023/01/88-anh-cho-husky-ngao-ngau-cute-de-thuong-hai-huoc_1.jpg"
          alt="husky2"
        />
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://zda.vn/wp-content/uploads/2023/01/88-anh-cho-husky-ngao-ngau-cute-de-thuong-hai-huoc_2.jpg"
          alt="husky1"
        />
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://mayvesinhmiennam.com/wp-content/uploads/2021/06/k3.jpg"
          alt="husky1"
        />
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://zda.vn/wp-content/uploads/2023/01/88-anh-cho-husky-ngao-ngau-cute-de-thuong-hai-huoc_1.jpg"
          alt="husky2"
        />
        <Image
          w="400px"
          h="300px"
          borderRadius={"6px"}
          src="https://zda.vn/wp-content/uploads/2023/01/88-anh-cho-husky-ngao-ngau-cute-de-thuong-hai-huoc_2.jpg"
          alt="husky1"
        />
      </SimpleGrid>
      <VStack spacing={4}>
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          Far far away, behind the word mountains, far from the countries
          Vokalia and Consonantia, there live the blind texts. Separated they
          live in Bookmarksgrove right at the coast of the Semantics, a large
          language ocean.
        </Text>
        <Text
          fontWeight={"400"}
          fontSize={"18px"}
          lineHeight="1.5"
          color={"#888"}
        >
          A small river named Duden flows by their place and supplies it with
          the necessary regelialia. It is a paradisematic country, in which
          roasted parts of sentences fly into your mouth.
        </Text>
      </VStack>
      <FooterBlog />
    </Box>
  );
};

export default DetailBlog;
