import { Box, Center, HStack, Text, VStack } from "@chakra-ui/layout";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay, Navigation, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/autoplay";
import { Image } from "@chakra-ui/react";

const MostPopularPost = () => {
  return (
    <Box mt="170px" w="100%">
      <Center mb="50px">
        <Text fontSize="40px " fontWeight="700">
          Most Popular Posts
        </Text>
      </Center>

      <Swiper
        style={{ paddingBottom: "60px", color: "yellowgreen" }}
        slidesPerView={1}
        slidesPerGroup={1}
        loop={true}
        navigation
        pagination={{
          clickable: true,
          type: "bullets",
          bulletActiveClass: "swiper-pagination-bullet-active",
          bulletClass: "swiper-pagination-bullet",
        }}
        modules={[Navigation, Pagination]}
        autoplay={{
          delay: 2000,
        }}
      >
        <SwiperSlide>
          <Box w="100%" display={"flex"}>
            <Image
              mr="50px"
              w="50%"
              h="385px"
              objectFit={"cover"}
              src="https://amazingdalat.com/media/images/thumbnail/hoa-oai-huong.jpg"
              alt="Flower"
            />
            <VStack alignItems={"start"} w="50%" spacing={4}>
              <HStack w="fit-content">
                <Text fontWeight="600" fontSize="14px">
                  Business, Travel
                </Text>
                <Text fontWeight="600" fontSize="14px">
                  — July 2, 2020
                </Text>
              </HStack>
              <Text fontWeight="700" fontSize="30px" w="fit-content">
                Your most unhappy customers are your greatest source of
                learning.
              </Text>
              <Text
                fontWeight={"400"}
                fontSize={"12px"}
                color="#888"
                w="fit-content"
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in Bookmarksgrove right at the coast of the Semantics,
                a large language ocean.
              </Text>

              <HStack>
                <Image
                  src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
                  alt="animals"
                  h="45px"
                  w="45px"
                  borderRadius={"50%"}
                  objectFit={"cover"}
                />
                <VStack alignItems={"start"}>
                  <Text fontWeight={"700"} lineHeight={"1"}>
                    Sergy Campbell
                  </Text>

                  <Text fontSize={"13px"} color={"#888"}>
                    CEO and Founder
                  </Text>
                </VStack>
              </HStack>
            </VStack>
          </Box>
        </SwiperSlide>
        <SwiperSlide>
          <Box w="100%" display={"flex"}>
            <Image
              mr="50px"
              w="50%"
              h="385px"
              objectFit={"cover"}
              src="https://amazingdalat.com/media/images/thumbnail/hoa-oai-huong.jpg"
              alt="Flower"
            />
            <VStack alignItems={"start"} w="50%">
              <HStack w="fit-content">
                <Text fontWeight="600" fontSize="14px">
                  Business, Travel
                </Text>
                <Text fontWeight="600" fontSize="14px">
                  — July 2, 2020
                </Text>
              </HStack>
              <Text fontWeight="700" fontSize="30px" w="fit-content">
                Your most unhappy customers are your greatest source of
                learning.
              </Text>
              <Text
                fontWeight={"400"}
                fontSize={"12px"}
                color="#888"
                w="fit-content"
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in Bookmarksgrove right at the coast of the Semantics,
                a large language ocean.
              </Text>

              <HStack mt="4px">
                <Image
                  src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
                  alt="animals"
                  h="45px"
                  w="45px"
                  borderRadius={"50%"}
                  objectFit={"cover"}
                />
                <VStack alignItems={"start"}>
                  <Text fontWeight={"700"} lineHeight={"1"}>
                    Sergy Campbell
                  </Text>

                  <Text fontSize={"13px"} color={"#888"}>
                    CEO and Founder
                  </Text>
                </VStack>
              </HStack>
            </VStack>
          </Box>
        </SwiperSlide>
        <SwiperSlide>
          <Box w="100%" display={"flex"}>
            <Image
              mr="50px"
              w="50%"
              h="385px"
              objectFit={"cover"}
              src="https://amazingdalat.com/media/images/thumbnail/hoa-oai-huong.jpg"
              alt="Flower"
            />
            <VStack alignItems={"start"} w="50%" spacing={4}>
              <HStack w="fit-content">
                <Text fontWeight="600" fontSize="14px">
                  Business, Travel
                </Text>
                <Text fontWeight="600" fontSize="14px">
                  — July 2, 2020
                </Text>
              </HStack>
              <Text fontWeight="700" fontSize="30px" w="fit-content">
                Your most unhappy customers are your greatest source of
                learning.
              </Text>
              <Text
                fontWeight={"400"}
                fontSize={"12px"}
                color="#888"
                w="fit-content"
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in Bookmarksgrove right at the coast of the Semantics,
                a large language ocean.
              </Text>

              <HStack>
                <Image
                  src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
                  alt="animals"
                  h="45px"
                  w="45px"
                  borderRadius={"50%"}
                  objectFit={"cover"}
                />
                <VStack alignItems={"start"}>
                  <Text fontWeight={"700"} lineHeight={"1"}>
                    Sergy Campbell
                  </Text>

                  <Text fontSize={"13px"} color={"#888"}>
                    CEO and Founder
                  </Text>
                </VStack>
              </HStack>
            </VStack>
          </Box>
        </SwiperSlide>
        <SwiperSlide>
          <Box w="100%" display={"flex"}>
            <Image
              mr="50px"
              w="50%"
              h="385px"
              objectFit={"cover"}
              src="https://amazingdalat.com/media/images/thumbnail/hoa-oai-huong.jpg"
              alt="Flower"
            />
            <VStack alignItems={"start"} w="50%" spacing={4}>
              <HStack w="fit-content">
                <Text fontWeight="600" fontSize="14px">
                  Business, Travel
                </Text>
                <Text fontWeight="600" fontSize="14px">
                  — July 2, 2020
                </Text>
              </HStack>
              <Text fontWeight="700" fontSize="30px" w="fit-content">
                Your most unhappy customers are your greatest source of
                learning.
              </Text>
              <Text
                fontWeight={"400"}
                fontSize={"12px"}
                color="#888"
                w="fit-content"
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in Bookmarksgrove right at the coast of the Semantics,
                a large language ocean.
              </Text>

              <HStack>
                <Image
                  src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
                  alt="animals"
                  h="45px"
                  w="45px"
                  borderRadius={"50%"}
                  objectFit={"cover"}
                />
                <VStack alignItems={"start"}>
                  <Text fontWeight={"700"} lineHeight={"1"}>
                    Sergy Campbell
                  </Text>

                  <Text fontSize={"13px"} color={"#888"}>
                    CEO and Founder
                  </Text>
                </VStack>
              </HStack>
            </VStack>
          </Box>
        </SwiperSlide>
        <SwiperSlide>
          <Box w="100%" display={"flex"}>
            <Image
              mr="50px"
              w="50%"
              h="385px"
              objectFit={"cover"}
              src="https://amazingdalat.com/media/images/thumbnail/hoa-oai-huong.jpg"
              alt="Flower"
            />
            <VStack alignItems={"start"} w="50%" spacing={4}>
              <HStack w="fit-content">
                <Text fontWeight="600" fontSize="14px">
                  Business, Travel
                </Text>
                <Text fontWeight="600" fontSize="14px">
                  — July 2, 2020
                </Text>
              </HStack>
              <Text fontWeight="700" fontSize="30px" w="fit-content">
                Your most unhappy customers are your greatest source of
                learning.
              </Text>
              <Text
                fontWeight={"400"}
                fontSize={"12px"}
                color="#888"
                w="fit-content"
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in Bookmarksgrove right at the coast of the Semantics,
                a large language ocean.
              </Text>

              <HStack>
                <Image
                  src="https://pethouse.com.vn/wp-content/uploads/2022/12/Ngoai-hinh-husky-768x1024-1.jpg"
                  alt="animals"
                  h="45px"
                  w="45px"
                  borderRadius={"50%"}
                  objectFit={"cover"}
                />
                <VStack alignItems={"start"}>
                  <Text fontWeight={"700"} lineHeight={"1"}>
                    Sergy Campbell
                  </Text>

                  <Text fontSize={"13px"} color={"#888"}>
                    CEO and Founder
                  </Text>
                </VStack>
              </HStack>
            </VStack>
          </Box>
        </SwiperSlide>
      </Swiper>
    </Box>
  );
};

export default MostPopularPost;
