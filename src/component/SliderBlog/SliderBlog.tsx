import { Box, HStack, Heading, Text, VStack } from "@chakra-ui/layout";
import { Image, Link } from "@chakra-ui/react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/autoplay";
import { FormatDatetime } from "../untils/fomat/FormatDatetime";
import { useRouter } from "next/router";
import { DataProps } from "@/pages/Blog";

const SliderBlog = ({ data }: { data: DataProps[] }) => {
  const router = useRouter();
  console.log(data);
  return (
    <Box w="1136px" mx="auto">
      <VStack
        w="100%"
        my="20px"
        alignItems={"center"}
        justifyContent={"center"}
      >
        <Heading fontSize="50px" fontWeight="700">
          Trending
        </Heading>
      </VStack>

      <Swiper
        spaceBetween={50}
        speed={800}
        slidesPerView={1}
        loop={true}
        autoplay={{
          delay: 1000,
        }}
        pagination={{
          clickable: true,
          type: "bullets",
          bulletActiveClass: "swiper-pagination-bullet-active",
          bulletClass: "swiper-pagination-bullet",
        }}
        modules={[Pagination, Autoplay]}
        style={{ paddingBottom: "60px" }}
      >
        {data.map((item, index) => (
          <SwiperSlide key={index}>
            <Box
              w="100%"
              display={"flex"}
              cursor={"pointer"}
              onClick={() => {
                router.push(`/Blog/${item.slug}`);
              }}
            >
              <Image
                mr="50px"
                w="50%"
                h="385px"
                objectFit={"cover"}
                src={item.background}
                alt="Flower"
                borderRadius={"10px"}
              />
              <VStack
                alignItems={"start"}
                w="50%"
                spacing={4}
                justifyContent={"center"}
              >
                <HStack w="fit-content">
                  <Text fontWeight="600" fontSize="14px">
                    {item.hashtag.job}
                  </Text>
                  <Text>—</Text>
                  <Text fontWeight="400" fontSize="14px" color={"#888"}>
                    {FormatDatetime(new Date(item.time))}
                  </Text>
                </HStack>
                <Text
                  fontWeight="700"
                  fontSize="30px"
                  w="fit-content"
                  color={"black"}
                >
                  {item.title}
                </Text>
                <Text
                  fontWeight={"400"}
                  fontSize={"12px"}
                  color="#888"
                  w="fit-content"
                >
                  {item.description}
                </Text>

                <HStack>
                  <Image
                    src={item.author.avatar}
                    alt="animals"
                    h="45px"
                    w="45px"
                    borderRadius={"50%"}
                    objectFit={"cover"}
                  />
                  <VStack alignItems={"start"}>
                    <Text fontWeight={"700"} lineHeight={"1"}>
                      {item.author.name}
                    </Text>

                    <Text fontSize={"13px"} color={"#888"}>
                      {item.author.position}
                    </Text>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
};

export default SliderBlog;
