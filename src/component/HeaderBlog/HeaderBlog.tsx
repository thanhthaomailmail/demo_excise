import {
  Box,
  Center,
  Divider,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Text,
} from "@chakra-ui/react";
import { ImTwitter } from "react-icons/im";
import { FaFacebookF, FaAlignJustify } from "react-icons/fa";
import { AiOutlineInstagram } from "react-icons/ai";
import { SearchIcon } from "@chakra-ui/icons";

const HeaderBlog = () => {
  return (
    <Box w="100%">
      <HStack w="1136px" mx="auto" pt="20px" mb="20px" spacing={200}>
        <InputGroup w="306px" h="38px">
          <InputLeftElement>
            <SearchIcon color="gray" />
          </InputLeftElement>
          <Input
            pl="20px"
            border="1px solid gray"
            placeholder="Search..."
            borderRadius={"40px"}
            _placeholder={{
              color: "gray",
            }}
            _focus={{
              border: "1px solid black",
            }}
          />
        </InputGroup>

        <Text
          fontSize="20px"
          color=" #000 !important"
          fontWeight={"700"}
          top={"6px"}
        >
          MAGDESIGN
        </Text>

        <HStack spacing={2}>
          <ImTwitter />
          <FaFacebookF />
          <AiOutlineInstagram />
        </HStack>
        <FaAlignJustify />
      </HStack>
      <Divider w="100%" borderBottom={"1px solid rgba(0, 0, 0, 0.1)"} />
    </Box>
  );
};

export default HeaderBlog;
