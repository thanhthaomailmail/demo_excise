import { GetServerSideProps } from "next";
import BlogUI from "@/src/component/Page/BlogUI";
import { fetchDataHomePage } from "@/src/lib/api";
import { lazy } from "react";
import { FieldsInput } from "@/src/lib/field";
import PreviewSuspense from "../../src/component/PreviewPanel/PreviewSuspense";

const BlogPreview = lazy(
  () => import("../../src/component/PreviewPanel/BlogPreview")
);

const query = `*[_type == "header_blog_input"]{${FieldsInput}}`;

export type DataProps = {
  title: string;
  slug: string;
  background: string;
  hashtag: { title: string; job: string };
  time: Date;
  content: any[];
  description: string;
  author: { name: string; position: string; avatar: string };
};
const Blog = ({ data, preview }: { data: DataProps[]; preview?: boolean }) => {
  return preview ? (
    <PreviewSuspense fallback="Loading...">
      <BlogPreview query={query} />
    </PreviewSuspense>
  ) : (
    <BlogUI data={data} />
  );
};

export default Blog;

export const getServerSideProps: GetServerSideProps = async ({
  preview = false,
}) => {
  if (preview) {
    return { props: { preview } };
  }
  const data = await fetchDataHomePage();
  return {
    props: {
      data,
      preview,
    },
  };
};
