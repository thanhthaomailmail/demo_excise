import DetailBlog from "@/src/component/DetailBlog/DetailBlog";
import { fetchInputField } from "@/src/lib/api";
import { GetServerSideProps } from "next";
import { DataProps } from ".";
import { lazy } from "react";
import PreviewSuspense from "../../src/component/PreviewPanel/PreviewSuspense";
import { FieldsInput } from "@/src/lib/field";

const BlogDetailPreview = lazy(
  () => import("../../src/component/PreviewPanel/BlogDetailPreview")
);
const query = `*[_type == "header_blog_input" && slug.current == $slug]{${FieldsInput}}`;

const Blog = ({
  data,
  preview,
  queryParams,
}: {
  data: DataProps;
  preview?: boolean;
  queryParams: {};
}) => {
  return preview ? (
    <PreviewSuspense fallback="Loading ...">
      <BlogDetailPreview query={query} queryParams={queryParams} />
    </PreviewSuspense>
  ) : (
    <DetailBlog data={data} />
  );
  // return (
  //   <Box w="100%" bg="#fff">
  //     <HeaderBlog />
  //     <DetailBlog data={data} />
  //     <FooterBlog />
  //   </Box>
  // );
};

export default Blog;

export const getServerSideProps: GetServerSideProps = async ({
  params,
  preview = false,
}) => {
  const queryParams = { slug: params?.slug ?? `` };
  if (preview) {
    return { props: { preview, queryParams } };
  }
  const data = await fetchInputField(params && params.slug);
  return {
    props: {
      data,
      preview,
      queryParams: {},
    },
  };
};
